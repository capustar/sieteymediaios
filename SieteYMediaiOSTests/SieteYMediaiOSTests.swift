//
//  SieteYMediaiOSTests.swift
//  SieteYMediaiOSTests
//
//  Created by mastermoviles on 27/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import XCTest

@testable import SieteYMediaiOS


class SieteYMediaiOSTests: XCTestCase
{
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCartaDatosCorrectos()
    {
        let carta = Carta(valor: 7, palo: .oros)!
        
        XCTAssertEqual(carta.palo, .oros)
    }
    
    func testCartaDatosIncorrectos()
    {
        let carta = Carta(valor: 8, palo: .oros)
        
        XCTAssertNil(carta)
    }
    
    func testRepartirBaraja()
    {
        let baraja = Baraja()
        let num_cartas_baraja_antes_repartir = baraja.cartas.count
        
        let carta_repartida = baraja.repartirCarta()
        let num_cartas_baraja_despues_repartir = baraja.cartas.count
        
        XCTAssertTrue(num_cartas_baraja_antes_repartir>num_cartas_baraja_despues_repartir, "Se ha repartido la carta");
        
        let presente = baraja.cartas.contains()
        {
            elemento in
            if elemento.palo == carta_repartida?.palo && elemento.valor == carta_repartida?.valor
            {
                return true
            }
            else
            {
                return false;
            }
        }
        
        XCTAssertFalse(presente, "La carta repartida ya no esta en la baraja");
    }
}
