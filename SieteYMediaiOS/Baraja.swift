//
//  Baraja.swift
//  SieteYMediaiOS
//
//  Created by mastermoviles on 27/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import Foundation

class Baraja
{
    var cartas = [Carta]()
    
    func barajar()
    {
        cartas.barajar()
    }
    
    func repartirCarta() -> Carta?
    {
        return cartas.popLast()
    }
    
    func rellenarBaraja()
    {
        for i in 1...12
        {
            if i != 8 && i != 9
            {
                cartas.append(Carta(valor: i, palo: .oros)!)
                cartas.append(Carta(valor: i, palo: .copas)!)
                cartas.append(Carta(valor: i, palo: .bastos)!)
                cartas.append(Carta(valor: i, palo: .espadas)!)
            }
        }
    }
    
    init()
    {
        rellenarBaraja()
        barajar()
    }
}
