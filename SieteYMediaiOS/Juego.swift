//
//  Juego.swift
//  SieteYMediaiOS
//
//  Created by mastermoviles on 27/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import Foundation

class Juego
{
    var baraja : Baraja
    var mano : Mano
    
    func sumarCartasMano() -> Double
    {
        let numero_cartas_mano = mano.tamaño
        var puntuacion = 0.0
        
        if numero_cartas_mano > 0
        {
            for i in 0...numero_cartas_mano - 1
            {
                let carta = mano.getCarta(posicion: i)
                
                if (carta?.valor)! < 10
                {
                    puntuacion += Double((carta?.valor)!)
                }
                else
                {
                    puntuacion += 0.5
                }
            }
        }
        
        return puntuacion
    }
    
    func pedirCarta()
    {
        let carta_nueva = baraja.repartirCarta()
        mano.addCarta(carta: carta_nueva!)
        print(carta_nueva!.descripcion())
        
        let nc = NotificationCenter.default
        nc.post(name: NSNotification.Name(rawValue: "nuevaCarta"), object: nil, userInfo: ["carta": carta_nueva!])
    }
    
    func plantarse()
    {
        let puntuacion_maquina = Double(arc4random_uniform(15))
        let puntuacion_usuario = sumarCartasMano()
        
        print("Tu puntuacion: " + String(format:"%.1f", puntuacion_usuario))
        print("Puntuacion de la maquina: " + String(format:"%.1f", puntuacion_maquina))
        
        if puntuacion_maquina <= 7.5 && (puntuacion_usuario > 7.5 || puntuacion_usuario == 0.0)
        {
            print("Ha ganado la maquina")
        }
        else if puntuacion_maquina <= 7.5 && puntuacion_maquina > 0 && (puntuacion_usuario > 7.5 || puntuacion_usuario == 0.0)
        {
            print("Empate")
        }
        else if puntuacion_usuario <= 7.5 && puntuacion_usuario > 0 && (puntuacion_maquina > 7.5 || puntuacion_maquina == 0.0)
        {
            print("Has ganado")
        }
        else if puntuacion_maquina > 7.5 && (puntuacion_usuario <= 0 || puntuacion_usuario > 7.5)
        {
            print("Empate")
        }
        else if puntuacion_maquina > 7.5 && puntuacion_usuario > 7.5
        {
            print("Empate")
        }
        else if puntuacion_maquina > puntuacion_usuario
        {
            print("Ha ganado la maquina")
        }
        else if puntuacion_usuario > puntuacion_maquina
        {
            print("Has ganado")
        }
        else if puntuacion_usuario == puntuacion_maquina
        {
            print("Empate")
        }
        else
        {
            print("Empate")
        }
        
        mano = Mano()
    }
    
    func acabarJuego()
    {
        
    }
    
    init()
    {
        baraja = Baraja()
        mano = Mano()
    }
}
