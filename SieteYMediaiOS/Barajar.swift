//
//  Barajar.swift
//  SieteYMediaiOS
//
//  Created by mastermoviles on 27/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import Foundation

extension Array
{
    mutating func barajar()
    {
        if count < 2
        {
            return
        }
        
        for i in indices.dropLast()
        {
            let diff = distance(from: i, to:endIndex)
            
            let j = index(i, offsetBy: numericCast(arc4random_uniform(numericCast(diff))))
            swapAt(i, j)
            
        }
    }
}
