//
//  ViewController.swift
//  SieteYMediaiOS
//
//  Created by mastermoviles on 23/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    var juego = Juego()
    var vistasCartas = [UIView]()
    var orden_cartas = 0
    
    func repartirCarta(carta: Carta, enPosicion : Int)
    {
        let nombreImagen = String(carta.valor)+String(carta.palo.rawValue)
        //creamos un objeto imagen
        let imagenCarta = UIImage(named: nombreImagen)
        //para que la imagen sea un componente más del UI,
        //la encapsulamos en un UIImageView
        let cartaView = UIImageView(image: imagenCarta)
        //Inicialmente la colocamos fuera de la pantalla y más grande
        //para que parezca más cercana
        //"frame" son los límites de la vista, definen pos y tamaño
        cartaView.frame = CGRect(x: -200, y: -200, width: 200, height: 300)
        //La rotamos, para que al "repartirla" haga un efecto de giro
        cartaView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
        //La añadimos a la vista principal, si no no sería visible
        self.view.addSubview(cartaView)
        //guardamos la vista en el array, para luego poder eliminarla
        self.vistasCartas.append(cartaView)
        //Animación de repartir carta
        UIView.animate(withDuration: 0.5)
        {
            //"efecto caida": la llevamos a la posición final
            cartaView.frame = CGRect(x:50+70*(enPosicion-1), y:100, width:70, height:100);
            //0 como ángulo "destino", para que rote mientras "cae"
            cartaView.transform = CGAffineTransform(rotationAngle:0);
        }
    }
    
    @IBAction func pedirCarta(_ sender: UIButton)
    {
        juego.pedirCarta()
        
    }
    
    @objc func recibir(notificacion:Notification)
    {
        if let userInfo = notificacion.userInfo
        {
            let carta_nueva = userInfo["carta"] as! Carta
            orden_cartas += 1            
            repartirCarta(carta: carta_nueva, enPosicion : orden_cartas)
        }
    }
    
    func borrarCartasPantalla()
    {
        for vistaCarta in self.vistasCartas
        {
            vistaCarta.removeFromSuperview()
        }
        
        self.vistasCartas=[]
        self.orden_cartas = 0
    }
    
    @IBAction func plantarse(_ sender: UIButton)
    {
        juego.plantarse()
        borrarCartasPantalla()
    }
    
    @IBAction func nuevaPartida(_ sender: UIButton)
    {
        juego = Juego()
        print("Nuevo juego")
        borrarCartasPantalla()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector:#selector(self.recibir), name:NSNotification.Name(rawValue:"nuevaCarta"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

