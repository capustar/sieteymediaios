//
//  Palo.swift
//  SieteYMediaiOS
//
//  Created by mastermoviles on 27/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import Foundation

enum Palo : String
{
    case oros
    case copas
    case bastos
    case espadas
}
