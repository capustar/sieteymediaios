//
//  Mano.swift
//  SieteYMediaiOS
//
//  Created by mastermoviles on 27/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import Foundation

class Mano
{
    var cartas = [Carta]()
    var tamaño = 0
    
    init()
    {
        
    }
    
    func addCarta(carta : Carta)
    {
        cartas.append(carta)
        tamaño = cartas.count
    }
    
    func getCarta(posicion : Int) -> Carta?
    {
        if posicion < 0 || posicion > tamaño
        {
            return nil
        }
        else
        {
            return cartas[posicion]
        }
    }
    
}
