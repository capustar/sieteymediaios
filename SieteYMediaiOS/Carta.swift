//
//  Carta.swift
//  SieteYMediaiOS
//
//  Created by mastermoviles on 27/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import Foundation

class Carta
{
    var valor : Int;
    var palo : Palo;
    
    init?(valor : Int, palo : Palo)
    {
        if valor != 8 && valor != 9 && valor > 0 && valor <= 12
        {
            self.valor = valor;
            self.palo = palo;
        }
        else
        {
            return nil;
        }
    }
    
    func descripcion() -> String
    {
        return "el \(self.valor) de \(self.palo)"
    }
    
    func getPuntos() -> Double
    {
        if self.valor < 10
        {
            return Double(self.valor)
        }
        else
        {
            return 0.5
        }
    }
}
